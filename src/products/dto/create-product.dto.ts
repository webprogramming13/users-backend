import { IsNotEmpty, MinLength, Matches } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @MinLength(5)
  name: string;

  @IsNotEmpty()
  price: number;
}
